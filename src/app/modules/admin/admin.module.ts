import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { CoreModule } from 'src/app/core/core.module';



@NgModule({
  declarations: [AdminComponent],
  imports: [
    CommonModule,
    CoreModule
  ]
})
export class AdminModule { }
